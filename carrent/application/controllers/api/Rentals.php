<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Rentals extends REST_Controller {

    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');	
		
		$this->load->model('m_rentals');
		$this->load->model('m_cars');
		$this->load->model('m_clients');
    }
	
	/**
	* create new Rental
	* method POST
	* API for insert new data Rental
	* input : 
	*  @data[car_id] : mandatory car ID
	*  @data[client_id] : mandatory client ID
	*  @data[date_from] : mandatory Tanggal dari
	*  @data[date_to] : mandatory Tanggal Sampai
	* 
	* return : rental id
	*/
	public function index_post()
    {
        $this->lang->load('form_validation');
		
        $data = $this->post('data');
		$error_message = Array();
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Rental'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(!isset($data["date_from"]) || empty($data["date_from"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Date From");
		}
		
		if(!isset($data["date_to"]) || empty($data["date_to"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Date To");
		}
		
		if($error_message == null){
			$valid_date = $this->m_rentals->valid_range_date($data["date_from"],$data["date_to"]);
			if(!$valid_date['status']){
				$this->response([
					'status' => FALSE,
					'message' => $valid_date['message']
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$data["date_from"] = date("Y-m-d", strtotime($data["date_from"]));
				$data["date_to"] = date("Y-m-d", strtotime($data["date_to"]));
			}
		}
		
        if(!isset($data["car_id"]) || empty($data["car_id"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Car");
		}else{
			$condition = array(
				"id" => $data["car_id"],
				"status" => 1
			);
			$valid_car = $this->m_cars->get_where($condition);
			if(!$valid_car){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid Car ID'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$valid_car = $this->m_rentals->car_in_rented($data["car_id"], $data["date_from"], $data["date_to"]);
				if($valid_car){
					$this->response([
						'status' => FALSE,
						'message' => 'Invalid Car, being in the rental'
					], REST_Controller::HTTP_BAD_REQUEST);
					return;
				}
			}
		}
		
		if(!isset($data["client_id"]) || empty($data["client_id"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Client");
		}else{
			$condition = array(
				"id" => $data["client_id"],
				"status" => 1
			);
			$valid_client = $this->m_clients->get_where($condition);
			if(!$valid_client){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid Client ID'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$valid_client = $this->m_rentals->client_in_rented($data["client_id"], $data["date_from"], $data["date_to"]);
				if($valid_client){
					$this->response([
						'status' => FALSE,
						'message' => 'Invalid Client, being in the rental'
					], REST_Controller::HTTP_BAD_REQUEST);
					return;
				}
			}
		}
        
		$rental_id = 0;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$data['status'] = 1;
			$rental_id = $this->m_rentals->save($data);
		}
		
        if ($rental_id){
            $result = Array("id" => $rental_id);
			$this->response([
                'status' => TRUE,
                'message' => 'Create Rental success.',
                'data' => $result
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Create Rental failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	
	/**
	* edit rental
	* method PUT
	* input : 
	*  @data[car_id] : mandatory car ID
	*  @data[client_id] : mandatory client ID
	*  @data[date_from] : mandatory Tanggal dari
	*  @data[date_to] : mandatory Tanggal Sampai
	* return : true/false
	*/
	public function index_put($id = 0)
    {
        $this->lang->load('form_validation');
        $data = $this->put('data');
		$column_update = array();
		
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Client'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$error_message = array();
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Rental ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$rental = $this->m_rentals->get_rentals_by_id($id);
		if(!$rental){
			$this->response([
				'status' => FALSE,
				'message' => 'Invalid Rental ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(!isset($data["date_from"]) || empty($data["date_from"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Date From");
		}
		
		if(!isset($data["date_to"]) || empty($data["date_to"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Date To");
		}
		
		if($error_message == null){
			$valid_date = $this->m_rentals->valid_range_date($data["date_from"],$data["date_to"]);
			if(!$valid_date['status']){
				$this->response([
					'status' => FALSE,
					'message' => $valid_date['message']
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$column_update["date_from"] = date("Y-m-d", strtotime($data["date_from"]));
				$column_update["date_to"] = date("Y-m-d", strtotime($data["date_to"]));
			}
		}
		
        if(!isset($data["car_id"]) || empty($data["car_id"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Car");
		}else{
			$condition = array(
				"id" => $data["car_id"],
				"status" => 1
			);
			$valid_car = $this->m_cars->get_where($condition);
			if(!$valid_car){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid Car ID'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$valid_car = $this->m_rentals->car_in_rented($data["car_id"], $data["date_from"], $data["date_to"],$id);
				if($valid_car){
					$this->response([
						'status' => FALSE,
						'message' => 'Invalid Car, being in the rental'
					], REST_Controller::HTTP_BAD_REQUEST);
					return;
				}else{
					$column_update['car_id'] = $data["car_id"];
				}
			}
		}
		
		if(!isset($data["client_id"]) || empty($data["client_id"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Client");
		}else{
			$condition = array(
				"id" => $data["client_id"],
				"status" => 1
			);
			$valid_client = $this->m_clients->get_where($condition);
			if(!$valid_client){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid Client ID'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$valid_client = $this->m_rentals->client_in_rented($data["client_id"], $data["date_from"], $data["date_to"],$id);
				if($valid_client){
					$this->response([
						'status' => FALSE,
						'message' => 'Invalid Client, being in the rental'
					], REST_Controller::HTTP_BAD_REQUEST);
					return;
				}else{
					$column_update['client_id'] = $data["client_id"];
				}
			}
		}
		
		$rental_id = $id;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$rental_id = $this->m_rentals->save($column_update, $rental_id);
		}
		
        if ($rental_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Edit Rental success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Edit Rental failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

	/**
	* delete Rental
	* Method DELETE
	* input : id rental
	* return : true/false
	*/
	public function index_delete($id = 0)
    {
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Rental ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		$column_update['status'] = 2;
		$rental_id = $this->m_rentals->save($column_update, $id);
		
		if ($rental_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Delete Rental success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Delete Rental failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
		
	}

	/**
	* list rental
	* Method GET
	* input : 
	* return : list of car rented
	*/
	public function index_get()
    {
		$condition = array(
			"status" => 1,
		);
		
		$rental = $this->m_rentals->get_rental($condition);
		if($rental && count($rental) > 0){
			$this->response([
				'status' => TRUE,
				'message' => 'List Rentals',
				'data' => $rental
			], REST_Controller::HTTP_OK);
			return;
		} else {
            $this->response([
                'status' => FALSE,
                'message' => 'List Rentals',
				'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
	}
}
