<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Cars extends REST_Controller {

    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');	
		
		$this->load->model('m_cars');
    }
	
	/**
	* create new Car
	* method POST
	* API for insert new data car
	* input : 
	*  @data[brand] : mandatory car brand
	*  @data[type] : mandatory car type
	*  @data[year] : mandatory car year
	*  @data[color] : mandatory car color
	*  @data[plare] : mandatory car plate
	* 
	* return : car id
	*/
	public function index_post()
    {
        $this->lang->load('form_validation');
		
        $data = $this->post('data');
		$error_message = Array();
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Car'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		
        if(!isset($data["brand"]) || empty($data["brand"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Brand");
		}
		if(!isset($data["type"]) || empty($data["type"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Type");
		}
		if (!isset($data["year"]) || empty($data["year"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Year");
		}else{
			$current_year = date("Y");
			if(intval($data["year"]) > intval($current_year)){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid year data'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}
		}
		if (!isset($data["color"]) || empty($data["color"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Color");
		}
        if (!isset($data["plate"]) || empty($data["plate"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Plate");
		} else {
			$condition = array("plate" => $data["plate"],"status" => 1);
			$car = $this->m_cars->get_where($condition);
			if($car && count($car) > 0){
				$this->response([
					'status' => FALSE,
					'message' => 'Car with no plate "'.$data["plate"].'" already exist'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}
		}
        
		$car_id = 0;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$data['status'] = 1;
			$car_id = $this->m_cars->save($data);
		}
		
        if ($car_id){
            $result = Array("id" => $car_id);
			$this->response([
                'status' => TRUE,
                'message' => 'Create Car success.',
                'data' => $result
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Create Car failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	
	/**
	* edit car
	* method PUT
	* input : 
	*  @data[brand] : optional car brand
	*  @data[type] : optional car type
	*  @data[year] : optional car year
	*  @data[color] : optional car color
	*  @data[plate] : optional car plate
	* return : true/false
	*/
	public function index_put($id = 0)
    {
        $this->lang->load('form_validation');
        $data = $this->put('data');
		$column_update = array();
		
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Car'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$error_message = array();
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Car ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$car = $this->m_cars->get_one($id);
		if(!$car){
			$this->response([
				'status' => FALSE,
				'message' => 'Invalid Car ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(!isset($data["brand"]) || empty($data["brand"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Brand");
		}else{
			$column_update["brand"] = $data["brand"];
		}
		
		if(!isset($data["type"]) || empty($data["type"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Type");
		}else{
			$column_update["type"] = $data["type"];
		}
		
		if (!isset($data["year"]) || empty($data["year"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Year");
		}else{
			$current_year = date("Y");
			if(intval($data["year"]) > intval($current_year)){
				$this->response([
					'status' => FALSE,
					'message' => 'Invalid year data'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}else{
				$column_update["year"] = $data["year"];
			}
		}
		
		if (!isset($data["color"]) || empty($data["color"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Color");
		}else{
			$column_update["color"] = $data["color"];
		}
		
        if(isset($data["plate"]) && !empty($data["plate"])){
			$condition = array(
				"plate" => $data["plate"],
				"id !=" => $id,
				"status" => 1,
			);
			$car = $this->m_cars->get_where($condition);

			if($car && count($car) > 0){
				$this->response([
					'status' => FALSE,
					'message' => 'Car with No Plate "'.$data["plate"].'" already exist'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			} else {
				$column_update["plate"] = $data["plate"];
			}
		}else{
			$error_message[] = sprintf($this->lang->line("isset"), "plate");
		}
		
		$car_id = $id;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$car_id = $this->m_cars->save($column_update, $car_id);
		}
		
        if ($car_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Edit Car success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Edit Car failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

	/**
	* delete car
	* Method DELETE
	* input : id car
	* return : true/false
	*/
	public function index_delete($id = 0)
    {
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Car ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		$column_update['status'] = 2;
		$car_id = $this->m_cars->save($column_update, $id);
		
		if ($car_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Delete Car success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Delete Car failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
		
	}

	/**
	* list car
	* Method GET
	* input : 
	* return : list of car
	*/
	public function index_get()
    {
		$condition = array(
			"status" => 1,
		);
		
		$cars = $this->m_cars->get_where($condition);
		if($cars && count($cars) > 0){
			$this->response([
				'status' => TRUE,
				'message' => 'List Car',
				'data' => $cars
			], REST_Controller::HTTP_OK);
			return;
		} else {
            $this->response([
                'status' => FALSE,
                'message' => 'List Car',
				'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
	}
	
	/**
	* list avalaible car by date
	* Method GET
	* input : date
	* return : list of car
	*/
	public function free_get()
    {
		$this->lang->load('form_validation');
		$date = $this->get('date');
		$error_message = Array();
		if(!isset($date) || empty($date)){
			$error_message[] = sprintf($this->lang->line("isset"), "Date");
		}else{
			$valid_date = $this->m_cars->validate_date($date);
			if($valid_date){
				$this->response([
					'status' => FALSE,
					'message' => 'Date format must be DD-MM-YYYY'
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
		
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$cars = $this->m_cars->get_status_cars($date,'free');
			if($cars && count($cars) > 0){
				$this->response([
					'status' => TRUE,
					'message' => 'List Car',
					'data' => Array(
						'date' => $date,
						'free_cars' => $cars
					)
				], REST_Controller::HTTP_OK);
				return;
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'List Car',
					'data' => null
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
	}
	
	/**
	* list car rented
	* Method GET
	* input : date
	* return : list of car
	*/
	public function rented_get()
    {
		$this->lang->load('form_validation');
		$date = $this->get('date');
		$error_message = Array();
		if(!isset($date) || empty($date)){
			$error_message[] = sprintf($this->lang->line("isset"), "Date");
		}else{
			$valid_date = $this->m_cars->validate_date($date);
			if($valid_date){
				$this->response([
					'status' => FALSE,
					'message' => 'Date format must be DD-MM-YYYY'
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
		
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$cars = $this->m_cars->get_status_cars($date,'rented');
			if($cars && count($cars) > 0){
				$this->response([
					'status' => TRUE,
					'message' => 'List Car',
					'data' => Array(
						'date' => $date,
						'rented_cars' => $cars
					)
				], REST_Controller::HTTP_OK);
				return;
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'List Car',
					'data' => null
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
	}
}
