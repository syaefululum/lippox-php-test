<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Clients extends REST_Controller {

    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');	
		
		$this->load->model('m_clients');
    }
	
	/**
	* create new Client
	* method POST
	* API for insert new data client
	* input : 
	*  @data[name] : mandatory client name
	*  @data[gender] : mandatory client gender
	* 
	* return : client id
	*/
	public function index_post()
    {
        $this->lang->load('form_validation');
		
        $data = $this->post('data');
		$error_message = Array();
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Client'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		
        if(!isset($data["name"]) || empty($data["name"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Name");
		}
		if(!isset($data["gender"]) || empty($data["gender"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Gender");
		}else{
			$gender_opt = $this->config->item("gender");
			if (!in_array(ucfirst($data["gender"]), $gender_opt)) {
				$this->response([
					'status' => FALSE,
					'message' => 'Gender value must be Male or Female'
				], REST_Controller::HTTP_BAD_REQUEST);
				return;
			}
			
			$data["gender"] = ucfirst($data["gender"]);
		}
        
		$client_id = 0;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$data['status'] = 1;
			$client_id = $this->m_clients->save($data);
		}
		
        if ($client_id){
            $result = Array("id" => $client_id);
			$this->response([
                'status' => TRUE,
                'message' => 'Create Client success.',
                'data' => $result
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Create Client failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }
	
	/**
	* edit client
	* method PUT
	* input : 
	*  @data[name] : optional client name
	*  @data[gender] : optional client gender
	* return : true/false
	*/
	public function index_put($id = 0)
    {
        $this->lang->load('form_validation');
        $data = $this->put('data');
		$column_update = array();
		
        if (!is_array($data) || count($data) == 0){
			$this->response([
                'status' => FALSE,
                'message' => 'Please provide data Client'
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$error_message = array();
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Client ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$client = $this->m_clients->get_one($id);
		if(!$client){
			$this->response([
				'status' => FALSE,
				'message' => 'Invalid Client ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(!isset($data["name"]) || empty($data["name"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Name");
		}else{
			$column_update["name"] = $data["name"];
		}
		
		if(!isset($data["gender"]) || empty($data["gender"])){
			$error_message[] = sprintf($this->lang->line("isset"), "Gender");
		}else{
			$column_update["gender"] = $data["gender"];
		}
		
		$client_id = $id;
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$client_id = $this->m_clients->save($column_update, $client_id);
		}
		
        if ($client_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Edit Client success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Edit Client failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
    }

	/**
	* delete client
	* Method DELETE
	* input : id client
	* return : true/false
	*/
	public function index_delete($id = 0)
    {
		$id = intval($id);
		if(!$id){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Client ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		$column_update['status'] = 2;
		$client_id = $this->m_clients->save($column_update, $id);
		
		if ($client_id){
            $this->response([
                'status' => TRUE,
                'message' => 'Delete Client success.'
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Delete Client failed.'
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
		
	}

	/**
	* list client
	* Method GET
	* input : 
	* return : list of clients
	*/
	public function index_get()
    {
		$condition = array(
			"status" => 1,
		);
		
		$car = $this->m_clients->get_where($condition);
		if($car && count($car) > 0){
			$this->response([
				'status' => TRUE,
				'message' => 'List Client',
				'data' => $car
			], REST_Controller::HTTP_OK);
			return;
		} else {
            $this->response([
                'status' => FALSE,
                'message' => 'List Client',
				'data' => null
            ], REST_Controller::HTTP_BAD_REQUEST);
        }
	}
}
