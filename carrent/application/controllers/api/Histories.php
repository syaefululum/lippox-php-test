<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Histories extends REST_Controller {

    function __construct()
    {
        parent::__construct();
		date_default_timezone_set('Asia/Jakarta');	
		
		$this->load->model('m_cars');
		$this->load->model('m_rentals');
		$this->load->model('m_clients');
    }
	
	/**
	* list history car rented
	* Method GET
	* input : id car, month
	* return : list of car
	*/
	public function car_get($id_car = 0)
    {
		$this->lang->load('form_validation');
		$month = $this->get('month');
		$error_message = Array();
		if(!isset($month) || empty($month)){
			$error_message[] = sprintf($this->lang->line("isset"), "Month");
		}else{
			$valid_month = $this->m_cars->validate_month($month);
			if($valid_month){
				$this->response([
					'status' => FALSE,
					'message' => 'Month format must be MM-YYYY'
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
		
		$id_car = intval($id_car);
		if(!$id_car){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Car ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$car = $this->m_cars->get_one($id_car);
		if(!$car){
			$this->response([
				'status' => FALSE,
				'message' => 'Invalid Car ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$cars = $this->m_rentals->get_car_history_rental($month);
			if($cars && count($cars) > 0){
				$this->response([
					'status' => TRUE,
					'message' => 'List Car',
					'data' => Array(
						'id' => $car->id,
						'brand' => $car->brand,
						'type' => $car->type,
						'plate' => $car->plate,
						'histories' => $cars
					)
				], REST_Controller::HTTP_OK);
				return;
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'List Car',
					'data' => null
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
	}
	
	/**
	* list history client rented
	* Method GET
	* input : id client
	* return : list of car
	*/
	public function client_get($id_client = 0)
    {
		$this->lang->load('form_validation');
		$error_message = Array();
		
		$id_client = intval($id_client);
		if(!$id_client){
			$this->response([
				'status' => FALSE,
				'message' => 'Please provide Client ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		$client = $this->m_clients->get_one($id_client);
		if(!$client){
			$this->response([
				'status' => FALSE,
				'message' => 'Invalid Client ID'
			], REST_Controller::HTTP_BAD_REQUEST);
			return;
		}
		
		if(count($error_message) > 0){
            $this->response([
                'status' => FALSE,
                'message' => (count($error_message) == 1 ? $error_message[0] : implode(", ", $error_message)),
            ], REST_Controller::HTTP_BAD_REQUEST);
			return;
		} else {
			$cars = $this->m_rentals->get_client_history_rental($id_client);
			if($cars && count($cars) > 0){
				$this->response([
					'status' => TRUE,
					'message' => 'List Car',
					'data' => Array(
						'id' => $client->id,
						'name' => $client->name,
						'gender' => $client->gender,
						'histories' => $cars
					)
				], REST_Controller::HTTP_OK);
				return;
			} else {
				$this->response([
					'status' => FALSE,
					'message' => 'List Car',
					'data' => null
				], REST_Controller::HTTP_BAD_REQUEST);
			}
		}
	}
}
