<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_cars extends MY_Model {
	protected $table = 'car';
	protected $common_column = "id,brand,type,year,color,plate";
	
	function get_status_cars($date,$type = ''){
		$date = date("Y-m-d", strtotime($date));
		$booked = $this->cars_booked($date,$type);
		
		$column = explode(",", $this->common_column);
		$this->db->select($column);
		$this->db->from($this->table);
		if($type == 'free'){
			if($booked != null){
				$this->db->where_not_in('id',$booked);
			}
		}else if($type == 'rented'){	
			$this->db->where_in('id',$booked);
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		
		return $result->result();
	}
	
	function get_free_cars($date){
		$date = date("Y-m-d", strtotime($date));
		$booked = $this->cars_booked($date);
		
		$column = explode(",", $this->common_column);
		$this->db->select($column);
		$this->db->from($this->table);
		if($booked != null){
			$this->db->where_not_in('id',$booked);
		}
		$this->db->where('status',1);
		$result = $this->db->get();
		return $result->result();
	}
	
	function validate_date($date)
	{
		$d = DateTime::createFromFormat('Y-m-d', $date);
		return $d && $d->format('Y-m-d') === $date;
	}
	
	function validate_month($month)
	{
		$d = DateTime::createFromFormat('Y-m', $month);
		return $d && $d->format('Y-m') === $month;
	}
	
}