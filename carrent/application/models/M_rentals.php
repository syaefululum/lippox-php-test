<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class m_rentals extends MY_Model {
	protected $table = 'rental';
	protected $common_column = "name,brand,type,plate,date_from,date_to";
	
	function valid_range_date($start_date, $end_date){
		$result = Array(
			'status' => true,
			'message' => ''
		);
		$today = date("Y-m-d");
		$start_date = date("Y-m-d", strtotime($start_date));
		$end_date = date("Y-m-d", strtotime($end_date));
		
		$format_start_date = new DateTime($start_date);
		$format_end_date = new DateTime($end_date);
		$interval = $format_start_date->diff($format_end_date);
		
		if($end_date < $start_date){
			$result['status'] = false;
			$result['message'] = "End date must be greater than Start date";
			return $result;
		}
		
		if(date('Y-m-d', strtotime($today . ' +1 day')) > $start_date ){
			$result['status'] = false;
			$result['message'] = "The start date should be one day from today";
			return $result;
		}
		
		if($interval->days > 3){
			$result['status'] = false;
			$result['message'] = "3 day maximum rental";
			return $result;
		}
		
		if(date('Y-m-d', strtotime($today . ' +7 day')) < $end_date){
			$result['status'] = false;
			$result['message'] = "The end date should be seven day from today";
			return $result;
		}
		
		return $result;
	}
	
	function client_in_rented($client_id, $start_date,$end_date,$rental_id = false){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('date_from >=',$start_date);
		$this->db->where('date_to <=',$end_date);
		$this->db->where('client_id',$client_id);
		$this->db->where('status',1);
		if($rental_id){
			$this->db->where('id !=',$rental_id);
		}
		$result = $this->db->get();
		return $result->result();
	}
	
	function car_in_rented($car_id, $start_date,$end_date){
		$this->db->select('*');
		$this->db->from($this->table);
		$this->db->where('date_from >=',$start_date);
		$this->db->where('date_to <=',$end_date);
		$this->db->where('car_id',$car_id);
		$this->db->where('status',1);
		$result = $this->db->get();
		return $result->result();
	}
	
	function get_rental(){
		$column = explode(",", $this->common_column);
		$this->db->select($column);
		$this->db->from($this->table);
		$this->db->where($this->table.'.status',1);
		$this->db->join('car','car.id = car_id');
		$this->db->join('client','client.id = client_id');
		$result = $this->db->get();
		return $result->result();
	}
	
	function get_car_history_rental($month){
		$column = Array(
			'client.name as rent_by',
			'date_from',
			'date_to',
		);
		
		$this->db->select($column);
		$this->db->from($this->table);
		$this->db->join('client','client.id = client_id');
		$this->db->where('DATE_FORMAT(date_from,"%m-%Y") >= ',$month);
		$this->db->where('DATE_FORMAT(date_to,"%m-%Y") <= ',$month);
		$result = $this->db->get();
		return $result->result();
	}
	
	function get_client_history_rental($client_id){
		$column = Array(
			'car.brand',
			'car.type',
			'car.plate',
			'date_from',
			'date_to',
		);
		
		$this->db->select($column);
		$this->db->from($this->table);
		$this->db->join('car','car.id = car_id');
		$this->db->where('client_id',$client_id);
		$result = $this->db->get();
		return $result->result();
	}
	
	function get_rentals_by_id($id){
		$column = explode(",", $this->common_column);
		$this->db->select($column);
		$this->db->from($this->table);
		$this->db->where($this->table.'.status',1);
		$this->db->where($this->table.'.id',$id);
		$this->db->join('car','car.id = car_id');
		$this->db->join('client','client.id = client_id');
		$result = $this->db->get();
		return $result->row();
	}
}